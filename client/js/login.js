const signin_btn = document.querySelector("#si-btn");
const signup_btn = document.querySelector("#su-btn");
const container = document.querySelector(".main-container");

signup_btn.addEventListener('click', ()=>{
    container.classList.add("signup-mode");
});

signin_btn.addEventListener('click', ()=>{
    container.classList.remove("signup-mode");
});